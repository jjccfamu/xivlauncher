**Welcome to the repository for the Final Fantasy XIV PowerShell launcher**

![screenshot](https://i.snag.gy/1nq2wu.jpg)

If you just want the download, click **Downloads** on the left side, followed by **Tags** on top and select the latest version.

---
## Features
  * Launch Final Fantasy XIV with a single click
  * Auto-launch after maintenance
  * Saves login credentials
  * Notifies if a patch is ready for download
  * Supports 2FA users
  * Can launch the game in English or German
  * [Optional] Automatically opens and closes Mudfish with the game
---
## Requirements
The launcher *just works* with Windows 10. For Windows 7 and 8, you will need to install the latest [Windows Management Framework](https://www.microsoft.com/en-us/download/details.aspx?id=54616). 

Additionally, if you wish to use the .ps1 (script) version instead of the .exe (executable) version, running PowerShell scripts on your system needs to be enabled - **if you get red text and the launcher immediately closes, the following applies to you**.

To enable running scripts, there are two options:

**Less secure**

1. Run Windows PowerShell as administrator

2. Type `Set-ExecutionPolicy Unrestricted`

3. Press enter

**More secure**

 1. Create a shortcut to the script file

 2. Right-click the shortcut and select properties 

 3. Click the Shortcut tab

 4. Place your cursor in the Target text box and remove all text

 5. Type `powershell -noprofile -executionpolicy bypass -file C:\Path\To\xivlauncher.ps1` where C:\Path\To\xivlauncher.ps1 is the path to the script file

 6. Press Okay

Note that you can add `-UseMudfish` to the end of that shortcut to have the launcher handle Mudfish: `powershell -noprofile -executionpolicy bypass -file C:\Path\To\xivlauncher.ps1 -UseMudfish`
You can also override the language choice with `-LanguageOverride`:  `powershell -noprofile -executionpolicy bypass -file C:\Path\To\xivlauncher.ps1 -LanguageOverride de`

## Credits
Code by jjccfamu

Testing and feedback by Nemekh and many others

German translation by Syntafin
