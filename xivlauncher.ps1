#region Initialize
# Parameters
param([switch]$UseMudfish,[switch]$UseSteam,[string]$LanguageOverride)
# Localization
$Messages = @{
    # In order of appearance
    en = @{
        WrongPSVer        = 'You are running too old of a powershell version for this script. Please install WMF v5.1 from https://www.microsoft.com/en-us/download/details.aspx?id=54616'
        InitialConnection = 'Contacting authorization portal'
        Error             = 'Error'
        ConnectionFailed  = 'SE servers could not be reached, trying again'
        Username          = 'Username'
        Password          = 'Password'
        OTP               = 'OTP (No OTP, Press Enter)'
        LoggingIn         = 'Logging in'
        LoginFailed       = 'Login failed, please try again'
        Authorizing       = 'Authorizing game launch'
        UpdateAvailable   = 'Update available for download'
        PatchServerDown   = 'Patch server is down'
        WaitingForMaint   = 'Waiting for maintenance to end'
        Launching         = 'Launching'
    }
    de = @{
        WrongPSVer        = 'Du verwendest eine veraltete, nicht mit dem script kompatible version von Powershell. Bitte installiere WMF v5.1 von https://www.microsoft.com/en-us/download/details.aspx?id=54616'
        InitialConnection = 'Kontaktiere login server'
        Error             = 'Fehler'
        ConnectionFailed  = 'SQEX server können nicht erreicht werden, versuche es nocheinmal'
        Username          = 'Benutzername'
        Password          = 'Passwort'
        OTP               = 'OTP (No OTP, Press Enter)'
        LoggingIn         = 'Einloggen'
        LoginFailed       = 'Login fehlgeschlagen, probiere es nocheinmal'
        Authorizing       = 'Authoriziere spielstart'
        UpdateAvailable   = 'Update zum download verfügbar'
        PatchServerDown   = 'Patch server ist nicht erreichbar'
        WaitingForMaint   = 'Warte das wartungsarbeiten beendet sind'
        Launching         = 'Starte'
    }
}
# Get system language, default to en if unknown
$SystemLanguage = if($LanguageOverride){$LanguageOverride}else{(Get-Culture).TwoLetterISOLanguageName}
if (-not $Messages[$SystemLanguage]) {
    $SystemLanguage = 'en'
}
# Check version
if ($PSVersionTable.PSVersion.Major -lt 3) {
    Write-Host -BackgroundColor DarkYellow -ForegroundColor Red -Object (Get-LocalizedString -Identifier 'WrongPSVer')
    $host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown') | Out-Null
    exit
}
if($UseMudfish.IsPresent -and -not ([System.Security.Principal.WindowsPrincipal]([System.Security.Principal.WindowsIdentity]::GetCurrent())).IsInRole([System.Security.Principal.WindowsBuiltInRole]::Administrator)) {
    # Pass on the arguments. This will *NOT* handle unparam'd args!
    $LaunchArgs = ("-noprofile -executionpolicy bypass -file `"{0}`" " -f (Convert-Path -Path $script:MyInvocation.MyCommand.Path))
    $LaunchArgs += $script:MyInvocation.BoundParameters.GetEnumerator() | Foreach {"-$($_.Key)", "$($_.Value)"}
    Start-Process powershell.exe -Verb RunAs -ArgumentList $LaunchArgs
    Start-Sleep -Milliseconds 333
    exit
}
# Make PowerShell Disappear
$PSWindow = @()
$PSWindow += '[DllImport("user32.dll")] public static extern bool ShowWindowAsync(IntPtr hWnd, int nCmdShow);'
$PSWindow += (Add-Type -MemberDefinition $PSWindow[0] -name Win32ShowWindowAsync -namespace Win32Functions -PassThru)
$PSWindow += (Get-Process -PID $pid).MainWindowHandle
$PSWindow[1]::ShowWindowAsync($PSWindow[2], 0)|Out-Null
# Ready SyncTable
$GlobalSyncTable = $null
$GlobalSyncTable = [hashtable]::Synchronized(@{})
# Import web functions
Add-Type -AssemblyName System.Web
$UAString = 'SQEXAuthor/2.0.0(Windows XP; ja-jp; 3aed65f87c)'
$ProgressPreference = 'SilentlyContinue'
# Set Global Variables
$Script:XIVUsername, $Script:XIVPassword, $Script:OneTimePasswordStatus, $Script:OneTimePassword, $Script:XIVPath, $Script:LoginSID, $Script:Region, $Script:Expansion, $Script:LastLoginTime = '', '', '', '', '', '', '', '', 0
$Script:Language = switch($SystemLanguage){
    ja {0}
    en {1}
    de {2}
    fr {3}
    default {1}
}
# Have we already saved preferences?
if(Test-Path -Path $env:TEMP\xivlauncher){
    $Script:XIVUsername, $Script:XIVPassword, $Script:OneTimePasswordStatus, $Script:XIVPath = Import-Clixml -Path $env:TEMP\xivlauncher
}
#region Initialize
#region Create-Runspace
function Create-Runspace {
    Param( 
        [ScriptBlock]$ScriptBlock,
        [Array]$Ars
    )
    $Pool = [RunspaceFactory]::CreateRunspace()
    $Pool.ApartmentState = 'STA'
    $Pool.ThreadOptions = 'ReuseThread'
    $Pool.Open()
    $Pool.SessionStateProxy.SetVariable('GlobalSyncTable', $GlobalSyncTable)
    $Session = [PowerShell]::Create()
    $Session.runspace = $Pool
    $null = $Session.AddScript($ScriptBlock)
    foreach($Ar in $Ars){
        $null = $Session.AddArgument($Ar)
    }
    return $Session
}
#region
#region XAML
[xml]$XAML = @'
<Window
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        Height="120" Width="600" ResizeMode="NoResize" Background="#FF1F1F1F" WindowStartupLocation="CenterScreen" Title="QuickLaunch" SnapsToDevicePixels="True" WindowStyle="None" ShowInTaskbar="False">
    <Grid x:Name="grid">
        <ProgressBar Name="progressBar" IsIndeterminate="True" Height="15" VerticalAlignment="Center" VerticalContentAlignment="Center" IsEnabled="False" BorderBrush="#00BCBCBC" Foreground="#4C0078D7" Background="#19FFFFFF" Margin="10,52.5"/>
        <TextBlock x:Name="UpperTextBlock" Margin="10,10,10,72.5" TextWrapping="Wrap" Text=" " Foreground="#FFF2F2F2" FontSize="20" VerticalAlignment="Bottom"/>
        <TextBlock x:Name="LowerTextBlock" Margin="10,72.5,10,10" TextWrapping="Wrap" Text=" " Foreground="#FFE81123" TextAlignment="Right" FontSize="16" VerticalAlignment="Top"/>
        <Button x:Name="button" Content="Button" Margin="600,0,0,120" IsCancel="True"/>
    </Grid>
</Window>
'@
#region 
#region RunSpace
$Runspace = (Create-Runspace -Scriptblock {
        Param([xml]$XAML)
        Add-Type -AssemblyName PresentationFramework, System.Drawing, System.Windows.Forms, WindowsFormsIntegration
        $GlobalSyncTable[0] = [Windows.Markup.XamlReader]::Load((New-Object System.Xml.XmlNodeReader $XAML))
        $XAML.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | % {$GlobalSyncTable.Add($_.Name, $GlobalSyncTable[0].FindName($_.Name) )}
        $GlobalSyncTable[0].Add_Closing({[System.Windows.Forms.Application]::Exit(); Start-Sleep -Seconds 1; Stop-Process $PID})
        $GlobalSyncTable.button.Add_Click({$GlobalSyncTable[0].Close()})
        [System.Windows.Forms.Integration.ElementHost]::EnableModelessKeyboardInterop($GlobalSyncTable[0])
        $GlobalSyncTable[0].Show()
        $GlobalSyncTable[0].Activate()
        $AppContext = New-Object System.Windows.Forms.ApplicationContext 
        [void][System.Windows.Forms.Application]::Run($AppContext)
    } -Ars $XAML)
$Runspace = [PSCustomObject]@{ Pipe = $Runspace; Status = $Runspace.BeginInvoke() }
#region
#region Functions
# Function to get a string in the correct language
function Get-LocalizedString {
    param([string]$Identifier)
    return $Messages[$SystemLanguage][$Identifier]
}
# Function to update text
function Write-Text {
    param($Element, $String)
    if($GlobalSyncTable.$Element.Text -ne $String){
        $GlobalSyncTable[0].Dispatcher.Invoke([action]{$GlobalSyncTable.$Element.Text = $String}, 'Normal')
    }
    # Start-Sleep -Milliseconds 66
}
# Function to generate file hashes
function Gen-Hash {
    param($FilePath)
    $Arg1 = [System.IO.Path]::GetFileName($FilePath)
    $Arg2 = ([System.IO.FileInfo]($FilePath)).Length
    $Arg3 = (Get-FileHash $FilePath -Algorithm SHA1).Hash.ToLower()
    return "$Arg1/$Arg2/$Arg3"
}
# Function for secure string -> plaintext
function ConvertFrom-SecureText {
    param($SecureString)
    return [System.Runtime.InteropServices.Marshal]::PtrToStringAuto([System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($SecureString))
}
# Function to get a certain file
function OpenFile-Dialog {
    # Load form stuff
    [System.Reflection.Assembly]::LoadWithPartialName('System.Windows.Forms') | Out-Null
    $FileDialog = New-Object System.Windows.Forms.OpenFileDialog
    # Start in SystemDrive:\
    # $FileDialog.InitialDirectory = $env:HOMEDRIVE
    # FFXIV/FFXIV_DX11.exe
    $FileDialog.filter = 'FFXIV|ffxiv.exe;ffxiv_dx11.exe'
    # Make sure they file they pick exists, cannot pick multiple files
    $FileDialog.CheckFileExists = $true
    $FileDialog.Multiselect = $false
    # Show dialog
    $FileDialog.ShowDialog() | Out-Null
    # Verify
    if(-not ($FileDialog.FileName)){
        OpenFile-Dialog
        return
    }
    # Return filename
    return $FileDialog.FileName
}
# Function to encode string in url format
function ConvertTo-URL {
    param($String)
    return [System.Web.HttpUtility]::UrlEncode($String)
}
# Function to convert a k,v,k,v,k,v string to a table
Function ConvertFrom-KvkvString{
    param( $str )
    $str = $str.Split(',')
    $tab = @{}
    for($i=0; $i -lt $str.Length; $i=$i+2){
        $tab.Add($str[$i],$str[$i+1])
    }
    return $tab
}

# Function to login to XIV
function Attempt-Login {
    # Get cookie
    try{
        Write-Text -Element 'UpperTextBlock' -String (Get-LocalizedString -Identifier 'InitialConnection')
        Write-Text -Element 'LowerTextBlock' -String ' '
        $EndpointURI = "https://ffxiv-login.square-enix.com/oauth/ffxivarr/login/top?lng=en&rgn=3&isft=0&issteam=$([int]$UseSteam.IsPresent)"
        $RequestData = Invoke-WebRequest -Method Get -Uri $EndpointURI -UserAgent $UAString -SessionVariable wrs
        $_STORED_ = [regex]::Match($RequestData.RawContent, '(?<=_STORED_" value=")([0-9a-f]*)').Value
    }
    catch{
        Write-Text -Element 'UpperTextBlock' -String (Get-LocalizedString -Identifier 'Error')
        Write-Text -Element 'LowerTextBlock' -String (Get-LocalizedString -Identifier 'ConnectionFailed')
        $RequestData, $_STORED_ = $null, $null
        Attempt-Login
    }
    if($Script:XIVUsername.Length -eq 0 -or $Script:XIVPassword.Length -eq 0){
        # Unhide window
        $PSWindow[1]::ShowWindowAsync($PSWindow[2], 1)|Out-Null
        $Script:XIVUsername = Read-Host (Get-LocalizedString -Identifier 'Username')
        # Gotta keep that password "secure"
        $Script:XIVPassword = Read-Host (Get-LocalizedString -Identifier 'Password') -AsSecureString
        # Rehide
        $PSWindow[1]::ShowWindowAsync($PSWindow[2], 0)|Out-Null
    }
    if($Script:OneTimePasswordStatus -ne 0 -and $Script:OneTimePassword.Length -eq 0){
        # Unhide window
        $PSWindow[1]::ShowWindowAsync($PSWindow[2], 1)|Out-Null
        $Script:OneTimePassword = Read-Host -Prompt (Get-LocalizedString -Identifier 'OTP')
        if($Script:OneTimePassword.Length -eq 0){
            $Script:OneTimePasswordStatus = 0
        }
        else{
            $Script:OneTimePasswordStatus = 1
        }
        # Rehide
        $PSWindow[1]::ShowWindowAsync($PSWindow[2], 0)|Out-Null
    }
    # Send our login, get temp SID
    try{
        Write-Text -Element 'UpperTextBlock' -String (Get-LocalizedString -Identifier 'LoggingIn')
        Write-Text -Element 'LowerTextBlock' -String ' '
        $RequestData = Invoke-WebRequest -Method Post -Uri 'https://ffxiv-login.square-enix.com/oauth/ffxivarr/login/login.send' -UserAgent $UAString -SessionVariable wrs -Body "_STORED_=$_STORED_&sqexid=$Script:XIVUsername&password=$(ConvertTo-URL (ConvertFrom-SecureText $Script:XIVPassword))&otppw=$Script:OneTimePassword"
        $FilteredRequestData = ( ConvertFrom-KvkvString ( [regex]::Match($RequestData.RawContent, '(?<="login=)[0-9a-z,]*').Value ) )
        $Script:LoginSID = $FilteredRequestData.Item('sid')
        $Script:Region = $FilteredRequestData.Item('region')
        $Script:Expansion = $FilteredRequestData.Item('maxex')
    }
    catch{}
    if($FilteredRequestData -match 'err'){
        Write-Text -Element 'UpperTextBlock' -String (Get-LocalizedString -Identifier 'Error')
        Write-Text -Element 'LowerTextBlock' -String "$([regex]::Match($FilteredRequestData, '(.+?err,)(.*)').Groups[2].Value)"
        Start-Sleep -Seconds 2
        $Script:XIVUsername, $Script:XIVPassword, $Script:OneTimePassword, $Script:OneTimePasswordStatus = '', '', '', ''
        Attempt-Login
    }
    elseif($FilteredRequestData.Length -eq 0){
        Write-Text -Element 'UpperTextBlock' -String (Get-LocalizedString -Identifier 'Error')
        Write-Text -Element 'LowerTextBlock' -String (Get-LocalizedString -Identifier 'LoginFailed')
        Start-Sleep -Seconds 2
        $Script:XIVUsername, $Script:XIVPassword, $Script:OneTimePassword, $Script:OneTimePasswordStatus = '', '', '', ''
        Attempt-Login
    }
    # Get our *real* SID
    try{
        Write-Text -Element 'UpperTextBlock' -String (Get-LocalizedString -Identifier 'Authorizing')
        Write-Text -Element 'LowerTextBlock' -String ' '
        $BasePath = "$(Split-Path $Script:XIVPath)\.."
        $EndpointURI = "https://patch-gamever.ffxiv.com/http/win32/ffxivneo_release_game/$(Get-Content $BasePath\game\ffxivgame.ver)/$Script:LoginSID"
        $RequestData = Invoke-WebRequest -Method Post -Uri $EndpointURI -UserAgent $UAString -SessionVariable wrs -Body "$(Gen-Hash $BasePath\boot\ffxivboot.exe),$(Gen-Hash $BasePath\boot\ffxivboot64.exe),$(Gen-Hash $BasePath\boot\ffxivlauncher.exe),$(Gen-Hash $BasePath\boot\ffxivlauncher64.exe),$(Gen-Hash $BasePath\boot\ffxivupdater.exe),$(Gen-Hash $BasePath\boot\ffxivupdater64.exe)" -TimeoutSec 10 -ErrorAction SilentlyContinue
        $Script:LoginSID = $RequestData.Headers['X-Patch-Unique-Id']
        if($RequestData.Headers['X-Repository'] -ne $null){
            Write-Text -Element 'LowerTextBlock' -String (Get-LocalizedString -Identifier 'UpdateAvailable')
        }
        else{
            Write-Text -Element 'LowerTextBlock' -String ' '
        }
    }
    catch{
        # We'll be here if the patch server is down, but non-400 status codes can also put us here which we don't want
        if($_.Exception.Status -eq 'Timeout'){
            Write-Text -Element 'LowerTextBlock' -String (Get-LocalizedString -Identifier 'PatchServerDown')
        }
        $Script:LoginSID = $null       
    }
    # Save login time
    $Script:LastLoginTime = [math]::Round((Get-Date -UFormat %s), 0)
}
function Wait-FFXIV{
    while($true){
        # If our cookie is invalid, relog
        if($Script:LastLoginTime + 180 -lt [math]::Round((Get-Date -UFormat %s), 0)){
            Attempt-Login
        }
        # every other
        # Write-Text -Element 'UpperTextBlock' -String 'Fetching server status'
        $ServerStatus = ConvertFrom-Json (Invoke-WebRequest 'https://frontier.ffxiv.com/worldStatus/gate_status.json').Content
        if($ServerStatus.status -eq 1){
            break
        }
        Start-Sleep -Milliseconds 750
        Write-Text -Element 'UpperTextBlock' -String (Get-LocalizedString -Identifier 'WaitingForMaint')
        Start-Sleep -Milliseconds 750
    }
}
#region Functions
#region Do
# Get FFXIV path
if($Script:XIVPath.Length -eq 0 -or -not (Test-Path $Script:XIVPath)){
    $Script:XIVPath = (OpenFile-Dialog)
}
# Wait for process #2 to init
while(-not $GlobalSyncTable[0].Show){
    Start-Sleep -Milliseconds 50
}
# Login
Attempt-Login
# Wait for maint to end
Wait-FFXIV
# If login failed
if($Script:LoginSID -eq $null){
    Attempt-Login
}
# Announce
Write-Text -Element 'UpperTextBlock' -String (Get-LocalizedString -Identifier 'Launching')
# Save data
$Script:XIVUsername, $Script:XIVPassword, $Script:OneTimePasswordStatus, $Script:XIVPath | Export-Clixml -Path $env:TEMP\xivlauncher
# Launch
if($UseMudfish.IsPresent){
    Start-Process -FilePath "${env:ProgramFiles(x86)}\Mudfish Cloud VPN\mudrun.exe" -WorkingDirectory (Split-Path "${env:ProgramFiles(x86)}\Mudfish Cloud VPN\mudrun.exe")  -ErrorAction SilentlyContinue
}
$GlobalSyncTable[0].Dispatcher.invoke([action]{$GlobalSyncTable[0].Hide()})
Start-Process -FilePath $Script:XIVPath -Wait -ArgumentList "language=$Script:Language DEV.TestSID=$Script:LoginSID DEV.MaxEntitledExpansionID=$Script:Expansion SYS.Region=$Script:Region DEV.IgnoreVer=1 IsSteam=$([int]$UseSteam.IsPresent)"# DEV.LobbyHost01=neolobby01.ffxiv.com DEV.LobbyPort01=54994 DEV.LobbyHost02=neolobby02.ffxiv.com DEV.LobbyPort02=54994
# Cleanup
if($UseMudfish){
    Stop-Process -Name mudfish -Force -ErrorAction SilentlyContinue
    Stop-Process -Name mudrun -Force -ErrorAction SilentlyContinue
    Stop-Process -Name mudflow -Force -ErrorAction SilentlyContinue
}
$GlobalSyncTable[0].Dispatcher.invoke([action]{$GlobalSyncTable[0].Close()}, 'Normal')
#endregion Do